package ro.deimios.cneechecker;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 *
 * @author Deimios
 */
public class httpsDownloader {

    private URL loginUrl;
    private URL downloadUrl;
    private String certPath;
    private String certPass;

    public httpsDownloader(URL loginUrl, URL downloadUrl, String certPath, String certPass) {
        this.loginUrl = loginUrl;
        this.downloadUrl = downloadUrl;
        this.certPath = certPath;
        this.certPass = certPass;
    }

    public String download() {
        String retVal = "";
        try {
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(new FileInputStream(certPath), certPass.toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, certPass.toCharArray());
            KeyManager[] kmfs = kmf.getKeyManagers();
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(kmfs, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                try {
                    HttpsURLConnection conn = (HttpsURLConnection) loginUrl.openConnection();
                    conn.setFollowRedirects(false);
                    conn.connect();
                    ArrayList<String[]> cookies = cookieHelper.getCookies(conn);
                    HttpsURLConnection conn2 = (HttpsURLConnection) loginUrl.openConnection();
                    String sendBackCookies = cookieHelper.getCookieString(cookies);
                    conn2.setRequestProperty("Cookie", sendBackCookies);
                    conn2.connect();
                    ArrayList<String[]> cookies2 = cookieHelper.getCookies(conn2);
                    cookies.addAll(cookies2);
                    sendBackCookies = cookieHelper.getCookieString(cookies);
                    HttpsURLConnection conn3 = (HttpsURLConnection) downloadUrl.openConnection();
                    conn3.setRequestProperty("Cookie", sendBackCookies);
                    conn3.connect();
                    InputStream is = conn3.getInputStream();
                    BufferedReader in = new BufferedReader(new InputStreamReader(is));
                    String inputLine;
                    //TODO parse html
                    String page = "";
                    while ((inputLine = in.readLine()) != null) {
                        page += inputLine;
                    }
                    retVal = page;

                    in.close();
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (KeyManagementException ex) {
                Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            Logger.getLogger(httpsDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retVal;
    }
}
