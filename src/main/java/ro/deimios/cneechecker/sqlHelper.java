package ro.deimios.cneechecker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Deimios
 */
public class sqlHelper {

    private Connection conn = null;

    public sqlHelper(String connectString) {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            conn = DriverManager.getConnection(connectString, "sa", "");
            Statement st = conn.createStatement();
            st.execute("CREATE TABLE IF NOT EXISTS msg(ID INTEGER NOT NULL PRIMARY KEY,SENDER VARCHAR(255),S_DATE VARCHAR(255),SUBJECT VARCHAR(255),READ BOOLEAN)");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(String queryString) {
        try {
            Statement st = conn.createStatement();
            st.executeUpdate(queryString);
        } catch (SQLException ex) {
            Logger.getLogger(sqlHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String[]> query(String queryString) {
        try {

            ArrayList<String[]> retVal = new ArrayList();
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery(queryString);
            ResultSetMetaData meta = result.getMetaData();
            int colmax = meta.getColumnCount();
            for (; result.next();) {
                String[] oneRow = new String[colmax];
                for (int column = 0; column < colmax; column++) {
                    oneRow[column] = result.getString(column + 1);
                }
                retVal.add(oneRow);
            }
            result.close();
            return retVal;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public Connection getConnetion() {
        return conn;
    }

    public void shutdown() {
        try {
            Statement st = conn.createStatement();
            st.execute("SHUTDOWN");
            conn.close();    // if there are no other open connection
        } catch (SQLException e) {
            System.out.println("ERROR Shutting down DB");
        }
    }
}
