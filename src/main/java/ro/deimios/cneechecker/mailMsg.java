package ro.deimios.cneechecker;

/**
 *
 * @author Deimios
 */
public class mailMsg {

    private int id;
    private String subject;
    private String sender;
    private String date;
    private boolean read;

    public mailMsg(int id, String subject, String sender, String date, boolean read) {
        this.id = id;
        this.subject = subject;
        this.sender = sender;
        this.date = date;
        this.read = read;
    }



    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
