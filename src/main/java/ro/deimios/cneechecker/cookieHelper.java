package ro.deimios.cneechecker;

import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author Deimios
 */
public class cookieHelper {

    public static ArrayList<String[]> getCookies(HttpsURLConnection conn) {
        ArrayList<String[]> retVal = new ArrayList<String[]>();
        String headerName = "";
        for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++) {
            if (headerName.equals("Set-Cookie")) {
                String cookie = conn.getHeaderField(i);
                String[] tmpCookies = cookie.split("; ");
                for (String cookiepair : tmpCookies) {
                    if (!cookiepair.startsWith("path") && !cookiepair.startsWith("expires") && cookiepair.contains("=")) {
                        retVal.add(cookiepair.split("="));
                    }
                }
            }
        }

        return retVal;
    }

    public static String getCookieString(ArrayList<String[]> cookies) {
        String retVal = "";
        for (int i = 0; i < cookies.size(); i++) {
            retVal += cookies.get(i)[0] + "=" + cookies.get(i)[1] + "; ";
        }
        return retVal;
    }
}
