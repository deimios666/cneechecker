package ro.deimios.cneechecker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Deimios
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Properties properties = new Properties();
        InputStream propertiesInputStream;

        try {

            propertiesInputStream = new FileInputStream("cneechecker.properties");

            properties.load(propertiesInputStream);

            httpsDownloader htd = new httpsDownloader(
                    new URL(properties.getProperty("url1")),
                    new URL(properties.getProperty("url2")),
                    properties.getProperty("cert"),
                    properties.getProperty("pass"));
            ArrayList<mailMsg> newMsgs = squirrelMangler.mangle(htd.download());
            if (newMsgs.size() > 0) {
                sqlHelper sqh = new sqlHelper("jdbc:hsqldb:file:" + properties.getProperty("database_filename"));
                int lastMsgId = Integer.valueOf(sqh.query("SELECT MAX(id) FROM msg").get(0)[0]);
                for (mailMsg newMsg : newMsgs) {
                    if (newMsg.getId() > lastMsgId) {
                        sqh.update("INSERT INTO msg(id,sender,s_date,subject,read) VALUES(" + newMsg.getId() + ",'" + newMsg.getSender() + "','" + newMsg.getDate() + "','" + newMsg.getSubject() + "',TRUE)");
                        mailHelper.sendMail(properties.getProperty("smtp_server"), properties.getProperty("smtp_sender"), properties.getProperty("smtp_to_1"), properties.getProperty("subject_prefix") + newMsg.getSubject() + properties.getProperty("subject_suffix"), properties.getProperty("body_prefix") + newMsg.getSubject() + properties.getProperty("body_suffix"));
                        mailHelper.sendMail(properties.getProperty("smtp_server"), properties.getProperty("smtp_sender"), properties.getProperty("smtp_to_2"), properties.getProperty("subject_prefix") + newMsg.getSubject() + properties.getProperty("subject_suffix"), properties.getProperty("body_prefix") + newMsg.getSubject() + properties.getProperty("body_suffix"));
                        mailHelper.sendMail(properties.getProperty("smtp_server"), properties.getProperty("smtp_sender"), properties.getProperty("smtp_to_3"), properties.getProperty("subject_prefix") + newMsg.getSubject() + properties.getProperty("subject_suffix"), properties.getProperty("body_prefix") + newMsg.getSubject() + properties.getProperty("body_suffix"));
                    }
                }
                sqh.shutdown();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
