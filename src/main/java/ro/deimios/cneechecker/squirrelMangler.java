package ro.deimios.cneechecker;

/**
 *
 * @author Deimios
 */
import java.util.ArrayList;
import org.apache.commons.lang3.StringEscapeUtils;

public class squirrelMangler {

    public static ArrayList<mailMsg> mangle(String page) {
        ArrayList<mailMsg> retVal = new ArrayList<>();
        String[] chunks = page.split("<label ");
        int msgid;
        boolean firstChunk = true;
        boolean newMsg;
        for (String chunk : chunks) {
            if (firstChunk) {
                firstChunk = false;
            } else {
                newMsg = false;
                chunk = chunk.split("</tr><tr")[0];
                String[] pieces = chunk.split("</td><td (.*?)>");
                msgid = Integer.valueOf(pieces[0].substring(8, pieces[0].indexOf(">", 0) - 1));
                String sender = pieces[0].substring(pieces[0].indexOf(">", 0) + 1, pieces[0].indexOf("</label", 0));
                String date = StringEscapeUtils.unescapeHtml4(pieces[1].replaceAll("<[^<,>]*>", ""));
                String subject = StringEscapeUtils.unescapeHtml4(pieces[3].replaceAll("<[^<,>]*>", ""));
                if (sender.contains("<b>")) {
                    newMsg = true;
                    sender = sender.replaceAll("<[^<,>]*>", "");
                }
                if (newMsg) {
                    retVal.add(new mailMsg(msgid, subject, sender, date, newMsg));
                    //System.out.println("\n================/ NEW \\====================");
                } else {
                    /*System.out.println("\n===========================================");*/
                }
                /*System.out.println("MsgID: " + msgid);
                System.out.println("From: " + sender);
                System.out.println("Date: " + date);
                System.out.println("Subject: " + subject);
                System.out.println("===========================================\n");*/
            }
        }
        return retVal;
    }
}
