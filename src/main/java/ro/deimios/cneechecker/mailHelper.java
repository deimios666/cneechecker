package ro.deimios.cneechecker;

/**
 *
 * @author Deimios
 */
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class mailHelper {

    public static void sendMail(String smtpServ, String from, String to, String subject, String message) {
        try {
            Properties props = System.getProperties();
            // -- Attaching to default Session, or we could start a new one --
            props.put("mail.transport.protocol", "smtp");
            //props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", smtpServ);
            //props.put("mail.smtp.auth", "true");
            //Authenticator auth = new SMTPAuthenticator();
            Session session = Session.getInstance(props);
            // -- Create a new message --
            Message msg = new MimeMessage(session);
            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(subject);
            msg.setText(message);
            // -- Set some other header information --
            //msg.setHeader("MyMail", "Mr. XYZ");
            msg.setSentDate(new Date());
            // -- Send the message --
            Transport.send(msg);
            //System.out.println("Message sent to" + to + " OK.");
        } catch (MessagingException ex) {
            System.out.println("Exception " + ex);
        }
    }
}
